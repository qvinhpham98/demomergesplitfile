//
//  FileTableViewCell.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/24/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "FileTableViewCell.h"

@implementation FileTableViewCell

- (void)setupSubviews {
    // Image View
//    self.typeFileImageView = [[UIImageView alloc] init];
//    self.typeFileImageView.contentMode = UIViewContentModeScaleAspectFill;
//    [self.contentView addSubview:self.typeFileImageView];
    
    // Title
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"ASDfadsfasdfasdf";
    self.titleLabel.textColor = UIColor.redColor;
    self.titleLabel.numberOfLines = 1;
    [self.contentView addSubview:self.titleLabel];
}


- (void)setupConstraints {
    
    self.typeFileImageView.translatesAutoresizingMaskIntoConstraints = false;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [NSLayoutConstraint activateConstraints:
     @[
       // Image View
//       [self.typeFileImageView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:10],
//       [self.typeFileImageView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-10],
//       [self.typeFileImageView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:10],
//       [self.typeFileImageView.heightAnchor constraintEqualToConstant:30],
//       [self.typeFileImageView.heightAnchor constraintEqualToAnchor:self.typeFileImageView.widthAnchor multiplier:1],
       
       // Title
       [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:30],
       [self.titleLabel.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-10],
       [self.titleLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:10],
       [self.titleLabel.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-10],
       ]
     ];
}


- (void)configureCellWith:(File *)file {
    self.titleLabel.text = file.name;
}

@end
