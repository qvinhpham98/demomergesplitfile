//
//  FileTableViewCell.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/24/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "File.h"

NS_ASSUME_NONNULL_BEGIN

@interface FileTableViewCell : BaseTableViewCell

@property (nonatomic, strong) UIImageView *typeFileImageView;
@property (nonatomic, strong) UILabel *titleLabel;

- (void)configureCellWith:(File *)file;

@end

NS_ASSUME_NONNULL_END
