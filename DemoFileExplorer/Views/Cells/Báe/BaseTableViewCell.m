//
//  BaseTableViewCell.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/13/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface BaseTableViewCell () {
    
}

@end

@implementation BaseTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubviews];
        [self setupConstraints];
    }
    return self;
}


- (void)setupSubviews { }


- (void)setupConstraints { }


+ (NSString *) reuseIdentifier {
    return NSStringFromClass([self class]);
}


@end
