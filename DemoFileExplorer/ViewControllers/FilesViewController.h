//
//  FilesViewController.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/24/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "File.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (strong, nonatomic) NSString *currentPath;
@property (strong, nonatomic) NSMutableArray<File *> *files;
@property (strong, nonatomic) NSMutableArray<File *> *selectedFiles;

@end

NS_ASSUME_NONNULL_END
