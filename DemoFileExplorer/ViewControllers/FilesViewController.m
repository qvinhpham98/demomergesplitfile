//
//  FilesViewController.m
//  AssignmentOne
//
//  Created by LAP12230 on 2/24/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "FilesViewController.h"
#import "FileTableViewCell.h"
#import "FileHelper.h"

@interface FilesViewController ()

@end

NSString * const ROOT_PATH = @"/Users/lap12230/Library/Developer/CoreSimulator/Devices/E21CFDE2-773B-4034-9D7F-96DB97054EEB/data";

@implementation FilesViewController

- (NSString *)currentPath {
    if (!_currentPath) {
        _currentPath = ROOT_PATH;
    }
    
    return _currentPath;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedFiles = [[NSMutableArray alloc] init];
    
    [self setupTableView];
    [self fetchData];
    [self setupObservers];
    
    NSLog(@"%@", self.currentPath);
}


- (void)setupTableView {
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.allowsMultipleSelection = true;
    [self.tableView registerClass:FileTableViewCell.class forCellReuseIdentifier:FileTableViewCell.reuseIdentifier];
}


- (void)fetchData {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray * paths = [fileManager contentsOfDirectoryAtPath:self.currentPath error:nil];
    
    self.files = [[NSMutableArray alloc] init];
    for (NSString *path in paths) {
        NSString *fullPath = [self.currentPath stringByAppendingPathComponent:path];
        File *file = [[File alloc] initWithPath:fullPath];
        [self.files addObject:file];
    }
}


- (void)setupObservers {
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(truncatingFileUpdateHandler:) name:kFileHelperUpdateTruncatingProgressNotification object:nil];
    [center addObserver:self selector:@selector(mergingFileUpdateHandler:) name:kFileHelperUpdateMergingProgressNotification object:nil];
}


- (void)truncatingFileUpdateHandler:(NSNotification *)notification {
    if (notification.name == kFileHelperUpdateTruncatingProgressNotification) {
        NSDictionary *dict = notification.userInfo;
        NSNumber *progress = dict[@"progress"];
        if (progress) {
            __weak typeof(self) weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.progressView setProgress:[progress floatValue]];
            });
        }
    }
}


- (void)mergingFileUpdateHandler:(NSNotification *)notification {
    if (notification.name == kFileHelperUpdateMergingProgressNotification) {
        NSDictionary *dict = notification.userInfo;
        NSNumber *progress = dict[@"progress"];
        if (progress) {
            __weak typeof(self) weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.progressView setProgress:[progress floatValue]];
            });
        }
    }
}


- (IBAction)didTapMergeButton:(UIBarButtonItem *)sender {
    
    NSMutableArray<NSString *> *paths = [[NSMutableArray alloc] init];
    for (File *file in self.selectedFiles) {
        [paths addObject:file.path];
    }
    
    // Validating
    if (paths.count == 0 ) return;
    
    NSString *dPath = [self.selectedFiles[0].path stringByDeletingLastPathComponent];
    [[FileHelper sharedInstance] mergeFilesAtPaths:paths destinationPath:dPath];
    
}


- (IBAction)didTapSplitButton:(id)sender {
    
    // Validate
    if (self.selectedFiles.count != 1) return;
    
    NSString *path = self.selectedFiles[0].path;
    NSString *dPath = [path stringByDeletingLastPathComponent];
    
    [[FileHelper sharedInstance] truncateFileAtPath:path destinationPath:dPath chunkSizeInMb:1000];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"Dealloc FilesViewController.");
}


#pragma - UITableView DataSources & Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.files.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FileTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:FileTableViewCell.reuseIdentifier];
    
    [cell configureCellWith:self.files[indexPath.item]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    File * selectedFile = self.files[indexPath.item];
    
    if (selectedFile.isDirectory) {
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        FilesViewController * vc = (FilesViewController *)[sb instantiateViewControllerWithIdentifier:@"FilesViewController"];
        
        if (vc) {
            vc.currentPath = self.files[indexPath.item].path;
            [self.navigationController pushViewController:vc animated:true];
        }
        
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:false];
        
    } else {
        
        [self.selectedFiles addObject:selectedFile];
        
    }
    
}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    File * deselectedFile = self.files[indexPath.item];
    [self.selectedFiles removeObject:deselectedFile];
    
    NSLog(@"%@", self.selectedFiles);
    
}


@end
