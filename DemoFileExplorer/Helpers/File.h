//
//  File.h
//  AssignmentOne
//
//  Created by LAP12230 on 2/21/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
// #import <SDWebImage/SDWebImage.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, FileAction) {
    FileActionReading,
    FileActionWriting,
};

@interface File : NSObject

@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) NSString *extension;
@property (readonly, nonatomic) NSString *path;
@property (readonly, nonatomic) NSURL *url;
@property (readonly, nonatomic) NSFileManager *defaultFileManager;
@property (readonly, nonatomic) BOOL isDirectory;

- (instancetype) initWithPath:(NSString *)path;
- (instancetype) initWithURL:(NSURL *) url;

- (BOOL)renameFileWithName:(NSString *)newName retainedOldExtension:(BOOL)retainedOldExtension;

- (BOOL)moveFileToPath:(NSString *)path;
- (BOOL)moveFileToURL:(NSURL *)url;

- (BOOL)copyFileToPath:(NSString *)path;
- (BOOL)copyFileToURL:(NSURL *)url;

- (void)performAction:(FileAction)action blockAction:(void (^ _Nonnull)(NSFileHandle *))blockAction;

- (void)loadImageFromPath:(NSString *)path completionBlock:(void (^ _Nullable)(UIImage *))completionBlock;

- (void)enumerateChild:(void (^)(File *))actionBlock;

- (NSArray<NSString *> *)truncateFileToDestinationPath:(NSString *)dPath chunkSizeInMb:(float)chunkSizeInMb progressHandler:(void (^_Nonnull)(double))progressHandler;

+ (void)truncateFileAtPath:(NSString *)path destinationPath:(NSString *)dPath chunkSizeInMb:(float)chunkSizeInMb progressHandler:(void (^_Nonnull)(double))progressHandler;

+ (void)mergeFilesAtPaths:(NSArray<NSString *> *)paths destinationPath:(NSString *)dPath progressHandler:(void (^_Nonnull)(double))progressHandler;


@end

NS_ASSUME_NONNULL_END
