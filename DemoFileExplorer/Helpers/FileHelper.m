//
//  FileHelper.m
//  DemoFileExplorer
//
//  Created by LAP12230 on 2/25/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "FileHelper.h"

static dispatch_once_t onceToken;
static FileHelper * sharedInstance;


@implementation FileHelper

NSString *const kFileHelperUpdateTruncatingProgressNotification = @"kFileHelperUpdateTruncatingProgressNotification";
NSString *const kFileHelperUpdateMergingProgressNotification = @"kFileHelperUpdateMergingProgressNotification";
NSString *const kFileHelperUpdateProgressNotification = @"kFileHelperUpdateProgressNotification";

+ (instancetype)sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[FileHelper alloc] init];
    });
    
    return sharedInstance;
}


- (void)truncateFileAtPath:(NSString *)path destinationPath:(NSString *)dPath chunkSizeInMb:(float)chunkSizeInMb {
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        if (self.operating) return;
        self.operating = true;
        
        // Validate path
        NSFileHandle *readingFileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
        if (!readingFileHandle) return;
        
        // Declaring
        NSFileManager *fileManager = NSFileManager.defaultManager;
        NSMutableArray<NSString *> *resultArray = [[NSMutableArray alloc] init];
        NSUInteger fileOrder = 1;
        NSString *fileName = [path lastPathComponent];
        
        unsigned long long offset = 0;
        unsigned long long totalSize = [readingFileHandle seekToEndOfFile];
        unsigned long long chunkSize = chunkSizeInMb * 1000 * 1000;
        unsigned long readingChunkSize = 8000;                                         // Small enough for optimizing ram while reading & writing.
        
        NSData *tempData;
        double nextProgressPercent = 1;
        while (offset < totalSize) {
            
            // Create new writing file chunk
            NSString *filePath;
            filePath = [dPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.data(%lu)", fileName, (unsigned long)fileOrder]];
            [fileManager createFileAtPath:filePath contents:nil attributes:nil];
            fileOrder += 1;
            [resultArray addObject:filePath];
            
            NSFileHandle *writingFileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
            
            // Loop reading and writing with small chunk size per time
            // => Avoid Ram, CPU overloading.
            while ([writingFileHandle seekToEndOfFile] < chunkSize && offset < totalSize) {
                @autoreleasepool {
                    [readingFileHandle seekToFileOffset:offset];
                    tempData = [readingFileHandle readDataOfLength:readingChunkSize];
                    [writingFileHandle writeData:tempData];
                    offset += [tempData length];
                    
                    // Update progress
                    double progress = (double)offset/totalSize;
                    if (progress >= nextProgressPercent / 100) {
                        nextProgressPercent = (int)(progress * 100) + 1;
                        
                        // Post notification
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFileHelperUpdateTruncatingProgressNotification object:self userInfo:@{@"progress": [NSNumber numberWithDouble:progress]}];
                    }
                }
            }
            
            [writingFileHandle closeFile];
        }
        
        [readingFileHandle closeFile];
        self.operating = false;
    });
}


- (void)mergeFilesAtPaths:(NSArray<NSString *> *)paths destinationPath:(NSString *)dPath {
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        if (self.operating) return;
        self.operating = true;
        
        // Get name of file
        NSString *fileName;
        if (paths[0]) {
            fileName = [NSString stringWithFormat:@"%@_merged", [[paths[0] lastPathComponent] stringByDeletingPathExtension]];
        }
        
        NSString * resultFilePath = [dPath stringByAppendingPathComponent:fileName];
        
        // Create empty result file
        NSFileManager *fileManager = NSFileManager.defaultManager;
        [fileManager createFileAtPath:resultFilePath contents:nil attributes:nil];
        
        // Get handler
        NSFileHandle *outputFileHandle = [NSFileHandle fileHandleForWritingAtPath:resultFilePath];
        
        // Merging
        double nextProgressPercent = 1;
        for (NSString *path in paths) {
            if ([fileManager fileExistsAtPath:path]) {
                
                NSFileHandle *readingFileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
                
                unsigned long long offset = 0;
                unsigned long long totalSize = [readingFileHandle seekToEndOfFile];
                unsigned long readingChunkSize = 8000;                              // Small enough for optimizing ram while reading & writing.
                
                while (offset < totalSize) {
                    @autoreleasepool {
                        [readingFileHandle seekToFileOffset:offset];
                        NSData *tempData = [readingFileHandle readDataOfLength:readingChunkSize];
                        offset += tempData.length;
                        [outputFileHandle writeData:tempData];
                        
                        // Update progress
                        double progress = (double)offset/totalSize;
                        if (progress >= nextProgressPercent / 100) {
                            nextProgressPercent = (int)(progress * 100) + 1;
                            
                            // Post notification
                            [[NSNotificationCenter defaultCenter] postNotificationName:kFileHelperUpdateMergingProgressNotification object:self userInfo:@{@"progress": [NSNumber numberWithDouble:progress]}];
                        }
                    }
                }
                
                [readingFileHandle closeFile];
            }
        }
        
        [outputFileHandle closeFile];
        self.operating = false;
    });
}


@end
